import { Injectable } from '@angular/core';
import { ITodo } from './interfaces/todo.interface';
import { AngularFirestore} from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  todoForm : ITodo;
  test:any;

  constructor(private firestore:AngularFirestore) {

  }

  getTodoList()
  {
    return this.firestore.collection('todos', x => x.orderBy('date', 'desc')).snapshotChanges();
  }

  getTodoListByDate()
  {
    return this.firestore.collection('todos', x => x.orderBy('date', 'asc')).snapshotChanges();
  }

  getTodoListByTextAsc()
  {
    return this.firestore.collection('todos', x => x.orderBy('text', 'asc')).snapshotChanges();
  }

  getTodoListByTextDesc()
  {
    return this.firestore.collection('todos', x => x.orderBy('text', 'desc')).snapshotChanges();
  }

  getTodoListByDescriptionAsc()
  {
    return this.firestore.collection('todos', x => x.orderBy('description', 'asc')).snapshotChanges();
  }

  getTodoListByDescriptionDesc()
  {
    return this.firestore.collection('todos', x => x.orderBy('description', 'desc')).snapshotChanges();
  }

  getTodoListByDoneAsc()
  {
    return this.firestore.collection('todos', x => x.orderBy('done', 'asc')).snapshotChanges();
  }

  getTodoListByDoneDesc()
  {
    return this.firestore.collection('todos', x => x.orderBy('done', 'desc')).snapshotChanges();
  }

  deleteAllDocuments(allIds:Array<string>)
  {
    for(let i = 0; i < allIds.length; i++)
    {
      this.firestore.collection('todos').doc(allIds[i]).delete();
    }

  }
  
}
