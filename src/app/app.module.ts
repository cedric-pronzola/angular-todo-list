import { BrowserModule } from '@angular/platform-browser';
import { CapitalizePipe } from "./capitalize.pipe";
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
registerLocaleData(localeFr, 'fr-FR', localeFrExtra);
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSortModule} from '@angular/material/sort';
import {MatInputModule} from '@angular/material/input';


import { AppComponent } from './app.component';
import { AllListsComponent } from './all-lists/all-lists.component';
import { TodoFormComponent } from './all-lists/todo-form/todo-form.component';
import { TodoService } from './todo.service';

@NgModule({
  declarations: [
    AppComponent,
    AllListsComponent,
    CapitalizePipe,
    TodoFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatButtonModule, 
    MatCheckboxModule,
    FormsModule,
    AngularFontAwesomeModule,
    NgbModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    MatSlideToggleModule,
    MatSortModule,
    MatInputModule
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
export class YourAppModule {}