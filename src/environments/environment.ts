// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAZzN9cO40E21sGMFVp9CpoY71y6t4oTZg",
    authDomain: "angular-todolist-974613.firebaseapp.com",
    databaseURL: "https://angular-todolist-974613.firebaseio.com",
    projectId: "angular-todolist-974613",
    storageBucket: "angular-todolist-974613.appspot.com",
    messagingSenderId: "728728795965",
    appId: "1:728728795965:web:ea1f57f231137c7a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
